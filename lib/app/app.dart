import 'package:flutter/material.dart';
//Redux packages
import 'package:built_redux/built_redux.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';

//Store
import 'package:yops_calm/domain/states/states.dart';
import 'package:yops_calm/domain/actions/actions.dart';

//Domain

//Containers

//Components - stupid elements without Redux

//Other
import 'package:yops_calm/app/theme.dart';
import 'package:yops_calm/navigation/routes.dart';
import 'package:yops_calm/navigation/nav_key.dart';

import 'package:yops_calm/presentation/presenters.dart';

import 'package:yops_calm/localization/i18n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:yops_calm/app/globals.dart' as globals;

class YopsCalmApp extends StatefulWidget {
  final bool loggedIn;
  YopsCalmApp({this.loggedIn});

  @override
  State<StatefulWidget> createState() {
    return new YopsCalmAppState();
  }
}

class YopsCalmAppState extends State<YopsCalmApp> {
  Store<AppState, AppStateBuilder, AppActions> store;

  var routes = {
    Routes.auth: (context) => HomeScreen(),
    Routes.home: (context) => HomeScreen(),
  };

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new ReduxProvider(
      store: globals.store,
      child: new MaterialApp(
        navigatorKey: NavKey.navKey,
        localizationsDelegates: [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: S.delegate.supportedLocales,
        title: "YOPS CALM",
        theme: AppTheme.appTheme,
        routes: routes,
        initialRoute: widget.loggedIn ? Routes.home : Routes.home,
      ),
    );
  }
}
