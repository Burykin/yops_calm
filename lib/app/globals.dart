library breeder.globals;

import 'dart:convert';

/// Redux
import 'package:yops_calm/domain/actions/actions.dart';
import 'package:yops_calm/domain/states/app_state.dart';
import 'package:yops_calm/domain/reducers/reducer_builder.dart';
import 'package:yops_calm/domain/middleware/middlewares.dart';
import 'package:yops_calm/domain/states/states.dart';
import 'package:built_redux/built_redux.dart';
import 'dart:async' show Future;

Store<AppState, AppStateBuilder, AppActions> _store;

Store<AppState, AppStateBuilder, AppActions> get store => _store;

setStore(store) => _store = store;

/// loggedIn status returned
Future<bool> initStore() async {
  //var state = UserState((b) => b);
  // try {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   final data = prefs.getString(USER_STATE_STORE_KEY) ?? "";

  //   if (data.isNotEmpty) {
  //     // state =
  //     //     serializers.deserializeWith(UserState.serializer, json.decode(data));
  //   }
  // } catch (e) {
  //   print(e.toString());
  // }

  _store = Store<AppState, AppStateBuilder, AppActions>(
      reducers, AppState((b) => b), AppActions(),
      middleware: middlewares);

  return true;
  //state.logingStatus;
}
