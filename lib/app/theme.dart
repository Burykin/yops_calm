import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppTheme {
  static final appTheme = new ThemeData.dark();

  static Color backgroundWhite = Color(0xFFFFFFFF);
  static Color backgroundGrey = Color(0xFFF3F3F3);

  static Color bottonBlue = Color(0xFF3A5886);
  static Color tabBlue = Color(0xFF295CF4).withOpacity(0.8);
  static Color tabGrey = Color(0xFFA5A29A);
  static Color textGrey = Color(0xFF9F9F9F);
  static Color white = Color(0xFFF3F3F3);
  static Color lightGrey = Color(0xFFADB1B8);
  static Color backgroundDark = Color(0xFF1A222C);
}

class NoSplashFactory extends InteractiveInkFeatureFactory {
  const NoSplashFactory();

  @override
  InteractiveInkFeature create({
    @required MaterialInkController controller,
    @required RenderBox referenceBox,
    @required Offset position,
    @required Color color,
    @required TextDirection textDirection,
    bool containedInkWell = false,
    RectCallback rectCallback,
    BorderRadius borderRadius,
    ShapeBorder customBorder,
    double radius,
    VoidCallback onRemoved,
  }) {
    return NoSplash(
      controller: controller,
      referenceBox: referenceBox,
    );
  }
}

class NoSplash extends InteractiveInkFeature {
  NoSplash({
    @required MaterialInkController controller,
    @required RenderBox referenceBox,
  })  : assert(controller != null),
        assert(referenceBox != null),
        super(
          controller: controller,
          referenceBox: referenceBox,
        );

  @override
  void paintFeature(Canvas canvas, Matrix4 transform) {}
}
