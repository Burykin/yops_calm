library app_tab;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:yops_calm/app/icons.dart';
import 'package:flutter/material.dart';

part 'app_tab.g.dart';

class AppTab extends EnumClass {
  static Serializer<AppTab> get serializer => _$appTabSerializer;

  static const AppTab main = _$main;
  static const AppTab sleep = _$sleep;
  static const AppTab meditate = _$meditate;
  static const AppTab music = _$music;

  const AppTab._(String name) : super(name);

  static BuiltSet<AppTab> get values => _$appTabValues;

  static AppTab valueOf(String name) => _$appTabValueOf(name);

  static AppTab fromIndex(int index) {
    switch (index) {
      case 0:
        return AppTab.main;
      case 1:
        return AppTab.sleep;
      case 2:
        return AppTab.meditate;
      case 3:
        return AppTab.music;
      default:
        return AppTab.main;
    }
  }

  static IconData iconFromName(String name) {
    switch (name) {
      case 'home':
        return Icons.home;
      case 'sleep':
        return Icons.hotel;
      case 'meditate':
        return Icons.brightness_medium;
      case 'music':
        return Icons.music_note;
      default:
        return Icons.home;
    }
  }

  static int toIndex(AppTab tab) {
    switch (tab) {
      case AppTab.main:
        return 0;
      case AppTab.sleep:
        return 1;
      case AppTab.meditate:
        return 2;
      case AppTab.music:
        return 3;
      default:
        return 0;
    }
  }
}
