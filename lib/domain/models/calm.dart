library calm;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'calm.g.dart';

abstract class Calm implements Built<Calm, CalmBuilder> {
  static Serializer<Calm> get serializer => _$calmSerializer;

  @nullable
  int get id;

  @nullable
  String get name;

  @nullable
  String get author;

  @nullable
  String get description;

  @nullable
  String get cashAddress;

  Calm._();

  factory Calm([updates(CalmBuilder b)]) = _$Calm;
}
