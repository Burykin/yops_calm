// GENERATED CODE - DO NOT MODIFY BY HAND

part of app_tab;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const AppTab _$main = const AppTab._('main');
const AppTab _$sleep = const AppTab._('sleep');
const AppTab _$meditate = const AppTab._('meditate');
const AppTab _$music = const AppTab._('music');

AppTab _$appTabValueOf(String name) {
  switch (name) {
    case 'main':
      return _$main;
    case 'sleep':
      return _$sleep;
    case 'meditate':
      return _$meditate;
    case 'music':
      return _$music;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<AppTab> _$appTabValues = new BuiltSet<AppTab>(const <AppTab>[
  _$main,
  _$sleep,
  _$meditate,
  _$music,
]);

Serializer<AppTab> _$appTabSerializer = new _$AppTabSerializer();

class _$AppTabSerializer implements PrimitiveSerializer<AppTab> {
  @override
  final Iterable<Type> types = const <Type>[AppTab];
  @override
  final String wireName = 'AppTab';

  @override
  Object serialize(Serializers serializers, AppTab object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  AppTab deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      AppTab.valueOf(serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
