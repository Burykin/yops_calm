// GENERATED CODE - DO NOT MODIFY BY HAND

part of calm;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Calm> _$calmSerializer = new _$CalmSerializer();

class _$CalmSerializer implements StructuredSerializer<Calm> {
  @override
  final Iterable<Type> types = const [Calm, _$Calm];
  @override
  final String wireName = 'Calm';

  @override
  Iterable serialize(Serializers serializers, Calm object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }
    if (object.name != null) {
      result
        ..add('name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.author != null) {
      result
        ..add('author')
        ..add(serializers.serialize(object.author,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add('description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.cashAddress != null) {
      result
        ..add('cashAddress')
        ..add(serializers.serialize(object.cashAddress,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  Calm deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CalmBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'author':
          result.author = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'cashAddress':
          result.cashAddress = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Calm extends Calm {
  @override
  final int id;
  @override
  final String name;
  @override
  final String author;
  @override
  final String description;
  @override
  final String cashAddress;

  factory _$Calm([void updates(CalmBuilder b)]) =>
      (new CalmBuilder()..update(updates)).build();

  _$Calm._(
      {this.id, this.name, this.author, this.description, this.cashAddress})
      : super._();

  @override
  Calm rebuild(void updates(CalmBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  CalmBuilder toBuilder() => new CalmBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Calm &&
        id == other.id &&
        name == other.name &&
        author == other.author &&
        description == other.description &&
        cashAddress == other.cashAddress;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), name.hashCode), author.hashCode),
            description.hashCode),
        cashAddress.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Calm')
          ..add('id', id)
          ..add('name', name)
          ..add('author', author)
          ..add('description', description)
          ..add('cashAddress', cashAddress))
        .toString();
  }
}

class CalmBuilder implements Builder<Calm, CalmBuilder> {
  _$Calm _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _author;
  String get author => _$this._author;
  set author(String author) => _$this._author = author;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _cashAddress;
  String get cashAddress => _$this._cashAddress;
  set cashAddress(String cashAddress) => _$this._cashAddress = cashAddress;

  CalmBuilder();

  CalmBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _author = _$v.author;
      _description = _$v.description;
      _cashAddress = _$v.cashAddress;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Calm other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Calm;
  }

  @override
  void update(void updates(CalmBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Calm build() {
    final _$result = _$v ??
        new _$Calm._(
            id: id,
            name: name,
            author: author,
            description: description,
            cashAddress: cashAddress);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
