import 'package:built_redux/built_redux.dart';
import 'package:yops_calm/domain/actions/actions.dart';
import 'package:yops_calm/domain/models/models.dart';

import 'package:yops_calm/domain/states/states.dart';

NestedReducerBuilder<AppState, AppStateBuilder, PlayerState, PlayerStateBuilder>
    createPlayerReducer() => new NestedReducerBuilder<AppState, AppStateBuilder,
            PlayerState, PlayerStateBuilder>(
        (state) => state.playerState, (builder) => builder.playerState)
      ..add<Calm>(PlayerActionsNames.playThisCalm, _playThisCalm)
      ..add<void>(PlayerActionsNames.play, _play)
      ..add<void>(PlayerActionsNames.pause, _pause)
      ..add<void>(PlayerActionsNames.open, _open);

void _playThisCalm(
    PlayerState state, Action<Calm> action, PlayerStateBuilder builder) {
  print("play calm");
  builder.playingCalm.replace(action.payload);
  builder.playing = true;
  builder.audioCache.play(action.payload.cashAddress);
}

void _play(PlayerState state, Action<void> action, PlayerStateBuilder builder) {
  builder.playing = true;
  builder.audioCache.fixedPlayer.resume();
}

void _pause(
    PlayerState state, Action<void> action, PlayerStateBuilder builder) {
  print("pause");
  builder.playing = false;
  builder.audioCache.fixedPlayer.pause();
}

void _open(PlayerState state, Action<void> action, PlayerStateBuilder builder) {
  builder.open = !state.open;
}
