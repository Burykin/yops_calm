import 'package:built_redux/built_redux.dart';
import 'package:yops_calm/domain/reducers/reducers.dart';

import 'package:yops_calm/domain/states/states.dart';

var reducerBuilder = new ReducerBuilder<AppState, AppStateBuilder>()
  ..combineNested(createTabReducer())
  ..combineNested(createPlayerReducer());

var reducers = reducerBuilder.build();
