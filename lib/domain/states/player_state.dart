library player_state;

import 'package:built_value/built_value.dart';
import 'package:built_collection/built_collection.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:built_value/serializer.dart';
import 'package:yops_calm/domain/models/models.dart';

part 'player_state.g.dart';

abstract class PlayerState implements Built<PlayerState, PlayerStateBuilder> {
  PlayerState._();
  @nullable
  Calm get playingCalm;

  AudioCache get audioCache;

  bool get playing;

  bool get open;

  factory PlayerState([updates(PlayerStateBuilder builder)]) =>
      new _$PlayerState((builder) => builder
        ..playing = false
        ..open = false
        ..audioCache = buildAudioPlayer()
        ..update(updates));

  static Serializer<PlayerState> get serializer => _$playerStateSerializer;

  static AudioCache buildAudioPlayer() {
    var player = AudioCache();
    var advancedPlayer = AudioPlayer();
    player.fixedPlayer = advancedPlayer;
    return player;
  }
}
