// GENERATED CODE - DO NOT MODIFY BY HAND

part of application;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AppState> _$appStateSerializer = new _$AppStateSerializer();

class _$AppStateSerializer implements StructuredSerializer<AppState> {
  @override
  final Iterable<Type> types = const [AppState, _$AppState];
  @override
  final String wireName = 'AppState';

  @override
  Iterable serialize(Serializers serializers, AppState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'tabState',
      serializers.serialize(object.tabState,
          specifiedType: const FullType(TabState)),
      'mainState',
      serializers.serialize(object.mainState,
          specifiedType: const FullType(MainState)),
      'playerState',
      serializers.serialize(object.playerState,
          specifiedType: const FullType(PlayerState)),
    ];

    return result;
  }

  @override
  AppState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AppStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'tabState':
          result.tabState.replace(serializers.deserialize(value,
              specifiedType: const FullType(TabState)) as TabState);
          break;
        case 'mainState':
          result.mainState.replace(serializers.deserialize(value,
              specifiedType: const FullType(MainState)) as MainState);
          break;
        case 'playerState':
          result.playerState.replace(serializers.deserialize(value,
              specifiedType: const FullType(PlayerState)) as PlayerState);
          break;
      }
    }

    return result.build();
  }
}

class _$AppState extends AppState {
  @override
  final TabState tabState;
  @override
  final MainState mainState;
  @override
  final PlayerState playerState;

  factory _$AppState([void updates(AppStateBuilder b)]) =>
      (new AppStateBuilder()..update(updates)).build();

  _$AppState._({this.tabState, this.mainState, this.playerState}) : super._() {
    if (tabState == null) {
      throw new BuiltValueNullFieldError('AppState', 'tabState');
    }
    if (mainState == null) {
      throw new BuiltValueNullFieldError('AppState', 'mainState');
    }
    if (playerState == null) {
      throw new BuiltValueNullFieldError('AppState', 'playerState');
    }
  }

  @override
  AppState rebuild(void updates(AppStateBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  AppStateBuilder toBuilder() => new AppStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AppState &&
        tabState == other.tabState &&
        mainState == other.mainState &&
        playerState == other.playerState;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, tabState.hashCode), mainState.hashCode),
        playerState.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AppState')
          ..add('tabState', tabState)
          ..add('mainState', mainState)
          ..add('playerState', playerState))
        .toString();
  }
}

class AppStateBuilder implements Builder<AppState, AppStateBuilder> {
  _$AppState _$v;

  TabStateBuilder _tabState;
  TabStateBuilder get tabState => _$this._tabState ??= new TabStateBuilder();
  set tabState(TabStateBuilder tabState) => _$this._tabState = tabState;

  MainStateBuilder _mainState;
  MainStateBuilder get mainState =>
      _$this._mainState ??= new MainStateBuilder();
  set mainState(MainStateBuilder mainState) => _$this._mainState = mainState;

  PlayerStateBuilder _playerState;
  PlayerStateBuilder get playerState =>
      _$this._playerState ??= new PlayerStateBuilder();
  set playerState(PlayerStateBuilder playerState) =>
      _$this._playerState = playerState;

  AppStateBuilder();

  AppStateBuilder get _$this {
    if (_$v != null) {
      _tabState = _$v.tabState?.toBuilder();
      _mainState = _$v.mainState?.toBuilder();
      _playerState = _$v.playerState?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AppState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AppState;
  }

  @override
  void update(void updates(AppStateBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$AppState build() {
    _$AppState _$result;
    try {
      _$result = _$v ??
          new _$AppState._(
              tabState: tabState.build(),
              mainState: mainState.build(),
              playerState: playerState.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'tabState';
        tabState.build();
        _$failedField = 'mainState';
        mainState.build();
        _$failedField = 'playerState';
        playerState.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AppState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
