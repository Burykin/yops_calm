// GENERATED CODE - DO NOT MODIFY BY HAND

part of main_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<MainState> _$mainStateSerializer = new _$MainStateSerializer();

class _$MainStateSerializer implements StructuredSerializer<MainState> {
  @override
  final Iterable<Type> types = const [MainState, _$MainState];
  @override
  final String wireName = 'MainState';

  @override
  Iterable serialize(Serializers serializers, MainState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.calms != null) {
      result
        ..add('calms')
        ..add(serializers.serialize(object.calms,
            specifiedType:
                const FullType(BuiltList, const [const FullType(Calm)])));
    }

    return result;
  }

  @override
  MainState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new MainStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'calms':
          result.calms.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Calm)]))
              as BuiltList);
          break;
      }
    }

    return result.build();
  }
}

class _$MainState extends MainState {
  @override
  final BuiltList<Calm> calms;

  factory _$MainState([void updates(MainStateBuilder b)]) =>
      (new MainStateBuilder()..update(updates)).build();

  _$MainState._({this.calms}) : super._();

  @override
  MainState rebuild(void updates(MainStateBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  MainStateBuilder toBuilder() => new MainStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MainState && calms == other.calms;
  }

  @override
  int get hashCode {
    return $jf($jc(0, calms.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MainState')..add('calms', calms))
        .toString();
  }
}

class MainStateBuilder implements Builder<MainState, MainStateBuilder> {
  _$MainState _$v;

  ListBuilder<Calm> _calms;
  ListBuilder<Calm> get calms => _$this._calms ??= new ListBuilder<Calm>();
  set calms(ListBuilder<Calm> calms) => _$this._calms = calms;

  MainStateBuilder();

  MainStateBuilder get _$this {
    if (_$v != null) {
      _calms = _$v.calms?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MainState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$MainState;
  }

  @override
  void update(void updates(MainStateBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$MainState build() {
    _$MainState _$result;
    try {
      _$result = _$v ?? new _$MainState._(calms: _calms?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'calms';
        _calms?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'MainState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
