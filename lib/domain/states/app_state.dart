library application;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:yops_calm/domain/states/states.dart';

part 'app_state.g.dart';

abstract class AppState implements Built<AppState, AppStateBuilder> {
  AppState._();

  factory AppState([updates(AppStateBuilder b)]) => new _$AppState((b) => b
    ..tabState = TabState((b) => b).toBuilder()
    ..mainState = MainState((b) => b).toBuilder()
    ..playerState = PlayerState((b) => b).toBuilder()
    ..update(updates));

  static Serializer<AppState> get serializer => _$appStateSerializer;

  TabState get tabState;
  MainState get mainState;
  PlayerState get playerState;
}
