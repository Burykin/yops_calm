library main_state;

import 'package:built_value/built_value.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:yops_calm/domain/models/models.dart';

part 'main_state.g.dart';

abstract class MainState implements Built<MainState, MainStateBuilder> {
  MainState._();
  @nullable
  BuiltList<Calm> get calms;

  factory MainState([updates(MainStateBuilder builder)]) =>
      new _$MainState((builder) => builder
        ..calms.update((updates) {
          Calm calm = Calm();

          final firstCalm = calm.rebuild((calmBuilder) {
            calmBuilder.id = 0;

            calmBuilder.author = "JoJo";
            calmBuilder.name = "Nature Night";
            calmBuilder.description = "Song recorded in night forest";
            calmBuilder.cashAddress = "sounds/night.mp3";
          });
          final secondCalm = calm.rebuild((calmBuilder) {
            calmBuilder.id = 1;
            calmBuilder.author = "JoJo";
            calmBuilder.name = "Nature Ocean";
            calmBuilder.description = "Song recorded in night around ocean";
            calmBuilder.cashAddress = "sounds/ocean.mp3";
          });
          updates.add(firstCalm);
          updates.add(secondCalm);
        })
        ..update(updates));

  static Serializer<MainState> get serializer => _$mainStateSerializer;
}
