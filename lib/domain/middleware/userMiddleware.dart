// import 'dart:convert';

// import 'package:built_redux/built_redux.dart';
// import 'package:yops_calm/domain/actions/actions.dart';
// import 'package:yops_calm/domain/serializers.dart';
// import 'package:yops_calm/domain/states/app_state.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:yops_calm/domain/states/states.dart';
// import 'package:yops_calm/domain/models/models.dart';
// import 'package:yops_calm/navigation/routes.dart';

// const USER_STATE_STORE_KEY = "USER_STATE_STORE_KEY";

// MiddlewareBuilder<AppState, AppStateBuilder, AppActions>
//     createUserMiddleware() =>
//         MiddlewareBuilder<AppState, AppStateBuilder, AppActions>()
//           ..add<void>(UserActionsNames.storeUserState, _storeUserState)
//           ..add<void>(UserActionsNames.restoreUserState, _restoreUserState)
//           ..add<UserProfile>(
//               UserProfileActionsNames.setUserProfile, _setUserProfile);

// void _storeUserState(MiddlewareApi<AppState, AppStateBuilder, AppActions> api,
//     ActionHandler next, Action<void> action) async {
//   next(action);

//   try {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     prefs.setString(
//         USER_STATE_STORE_KEY,
//         json.encode(serializers.serializeWith(
//             UserState.serializer, api.state.userState)));
//   } catch (e) {
//     print(e.toString());
//   }
// }

// void _restoreUserState(MiddlewareApi<AppState, AppStateBuilder, AppActions> api,
//     ActionHandler next, Action<void> action) async {
//   next(action);
//   SharedPreferences prefs = await SharedPreferences.getInstance();
//   final data = prefs.getString(USER_STATE_STORE_KEY) ?? "";
//   if (data.isNotEmpty) {
//     try {
//       final state =
//           serializers.deserializeWith(UserState.serializer, json.decode(data));
//       api.actions.user.setUserState(state);
//     } catch (e) {
//       print(e.toString());
//     }
//   }
// }

// void _setUserProfile(MiddlewareApi<AppState, AppStateBuilder, AppActions> api,
//     ActionHandler next, Action<UserProfile> action) async {
//   next(action);
//   final UserProfile profile = action.payload;
//   if (profile.firstName == null ||
//       profile.lastName == null ||
//       profile.email == null ||
//       profile.phone == null ||
//       profile.city == null ||
//       profile.phone == null) {
//     // api.actions
//     //     .routeTo(AppRoute((builder) => builder..route = Routes.fillProfile));
//   }
// }
