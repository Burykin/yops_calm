import 'package:built_redux/built_redux.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:yops_calm/domain/actions/actions.dart';
import 'package:yops_calm/domain/states/app_state.dart';
import 'package:yops_calm/navigation/routes.dart';
import 'package:yops_calm/navigation/nav_key.dart';

Store store;

MiddlewareBuilder<AppState, AppStateBuilder, AppActions>
    navigationMiddleware() {
  return MiddlewareBuilder<AppState, AppStateBuilder, AppActions>()
    ..add(AppActionsNames.routeTo, routeTo);
}

void routeTo(MiddlewareApi<AppState, AppStateBuilder, AppActions> api,
    ActionHandler next, Action<AppRoute> action) async {
  next(action);
  debugPrint("#routeTo middleware ");
  final payload = action.payload;

  // Navigator.of(action.payload.context).pushNamed('/details');
  switch (payload.route) {
    case Routes.auth:
      final navigatorKey = NavKey.navKey;
      navigatorKey.currentState.pushNamedAndRemoveUntil(
          Routes.auth, ModalRoute.withName(Routes.auth));
      break;
    case Routes.home:
      Navigator.of(payload.context, rootNavigator: true)
          .pushNamedAndRemoveUntil(
              Routes.home, ModalRoute.withName(Routes.home));

      break;
  }
}
