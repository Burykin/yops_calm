import 'package:flutter/material.dart' show BuildContext;
import 'package:built_value/built_value.dart';

part 'app_route.g.dart';

abstract class AppRoute implements Built<AppRoute, AppRouteBuilder> {
  String get route;

  @nullable
  String get payload;

  @nullable
  String get screenTitle;

  @nullable
  BuildContext get context;

  AppRoute._();

  factory AppRoute([updates(AppRouteBuilder buider)]) = _$AppRoute;
}
