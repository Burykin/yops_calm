import 'package:built_redux/built_redux.dart';
import 'package:yops_calm/domain/models/models.dart';
part 'player_actions.g.dart';

abstract class PlayerActions extends ReduxActions {
  PlayerActions._();

  ActionDispatcher<Calm> get playThisCalm;
  ActionDispatcher<void> get pause;
  ActionDispatcher<void> get play;
  ActionDispatcher<void> get open;

  factory PlayerActions() => new _$PlayerActions();
}
