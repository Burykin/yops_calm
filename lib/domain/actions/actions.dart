library actions;

import 'package:built_redux/built_redux.dart';

import 'app_route.dart';
export 'app_route.dart';

import 'tab_actions.dart';
export 'tab_actions.dart';
import 'player_actions.dart';
export 'player_actions.dart';

part 'actions.g.dart';

abstract class AppActions extends ReduxActions {
  AppActions._();

  TabActions get tab;
  PlayerActions get player;

  ActionDispatcher<AppRoute> get routeTo;

  factory AppActions() => new _$AppActions();
}
