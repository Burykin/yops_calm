// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'player_actions.dart';

// **************************************************************************
// BuiltReduxGenerator
// **************************************************************************

// ignore_for_file: avoid_classes_with_only_static_members
// ignore_for_file: annotate_overrides

class _$PlayerActions extends PlayerActions {
  factory _$PlayerActions() => new _$PlayerActions._();
  _$PlayerActions._() : super._();

  final ActionDispatcher<Calm> playThisCalm =
      new ActionDispatcher<Calm>('PlayerActions-playThisCalm');
  final ActionDispatcher<void> pause =
      new ActionDispatcher<void>('PlayerActions-pause');
  final ActionDispatcher<void> play =
      new ActionDispatcher<void>('PlayerActions-play');
  final ActionDispatcher<void> open =
      new ActionDispatcher<void>('PlayerActions-open');

  @override
  void setDispatcher(Dispatcher dispatcher) {
    playThisCalm.setDispatcher(dispatcher);
    pause.setDispatcher(dispatcher);
    play.setDispatcher(dispatcher);
    open.setDispatcher(dispatcher);
  }
}

class PlayerActionsNames {
  static final ActionName<Calm> playThisCalm =
      new ActionName<Calm>('PlayerActions-playThisCalm');
  static final ActionName<void> pause =
      new ActionName<void>('PlayerActions-pause');
  static final ActionName<void> play =
      new ActionName<void>('PlayerActions-play');
  static final ActionName<void> open =
      new ActionName<void>('PlayerActions-open');
}
