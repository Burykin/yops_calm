// GENERATED CODE - DO NOT MODIFY BY HAND

part of actions;

// **************************************************************************
// BuiltReduxGenerator
// **************************************************************************

// ignore_for_file: avoid_classes_with_only_static_members
// ignore_for_file: annotate_overrides

class _$AppActions extends AppActions {
  factory _$AppActions() => new _$AppActions._();
  _$AppActions._() : super._();

  final ActionDispatcher<AppRoute> routeTo =
      new ActionDispatcher<AppRoute>('AppActions-routeTo');
  final TabActions tab = new TabActions();
  final PlayerActions player = new PlayerActions();

  @override
  void setDispatcher(Dispatcher dispatcher) {
    routeTo.setDispatcher(dispatcher);
    tab.setDispatcher(dispatcher);
    player.setDispatcher(dispatcher);
  }
}

class AppActionsNames {
  static final ActionName<AppRoute> routeTo =
      new ActionName<AppRoute>('AppActions-routeTo');
}
