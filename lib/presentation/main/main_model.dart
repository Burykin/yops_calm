import 'package:yops_calm/domain/states/states.dart';
import 'package:yops_calm/domain/models/models.dart';
import 'package:built_collection/built_collection.dart';

class MainModel {
  final AppState appState;
  MainModel(this.appState);

  BuiltList<Calm> get calms => appState.mainState.calms;
  Calm get playingCalm => appState.playerState.playingCalm;
  PlayerState get player => appState.playerState;

  bool playing(Calm calm) {
    if (player.playingCalm != null) {
      if (calm.id == player.playingCalm.id && player.playing) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
