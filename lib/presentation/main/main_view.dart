import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:yops_calm/app/theme.dart';

import 'package:yops_calm/navigation/routes.dart';

import 'package:built_collection/built_collection.dart';
import 'package:yops_calm/domain/models/models.dart';
import 'widgets/widgets.dart';

import 'dart:async';

import 'main_model.dart';

class MainView extends StatefulWidget {
  final MainModel model;
  final Function(String route) routeTo;
  final Function(Calm calm) playThisCalm;
  final Function pause;
  final Function play;

  MainView(
      {this.model,
      this.routeTo,
      @required this.playThisCalm,
      @required this.pause,
      @required this.play});
  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: _body());
  }

  Widget _body() {
    return Column(children: <Widget>[
      Expanded(
          child: Stack(
        children: <Widget>[
          GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: CustomScrollView(slivers: <Widget>[
                SliverAppBar(
                  title: Container(
                      height: 100,
                      child: Center(
                          child: Text("YOPS CALM",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 25)))),
                ),
                SliverList(
                    delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                    return Padding(
                        padding: EdgeInsets.only(
                            left: 10,
                            right: 10,
                            top: index == 0 ? 10 : 0,
                            bottom: 10),
                        child: CalmWidget(
                          calm: widget.model.calms[index],
                          playing:
                              widget.model.playing(widget.model.calms[index]),
                          playThisCalm: widget.playThisCalm,
                          pause: widget.pause,
                        ));
                  },
                  childCount: widget.model.calms.length,
                ))
              ])),
        ],
      )),
    ]);
  }
}
