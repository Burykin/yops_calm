import 'package:yops_calm/domain/actions/actions.dart';
import 'package:yops_calm/domain/states/states.dart';
import 'package:yops_calm/domain/models/models.dart';

import 'package:flutter/material.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';
import 'main_model.dart';
import 'main_view.dart';

class MainContainer extends StoreConnector<AppState, AppActions, MainModel> {
  MainContainer({
    Key key,
  }) : super(key: key);

  @override
  MainModel connect(AppState state) => MainModel(state);

  @override
  Widget build(BuildContext context, MainModel model, AppActions actions) {
    return MainView(
      model: model,
      playThisCalm: (Calm calm) {
        _playCalm(calm, actions, model.appState);
      },
      pause: () {
        _pause(actions);
      },
      play: () {
        _play(actions, model.appState);
      },
    );
  }

  void _playCalm(Calm calm, AppActions actions, AppState state) {
    if (state.playerState?.playingCalm != null) {
      if (state.playerState?.playingCalm?.id != calm.id) {
        actions.player.playThisCalm(calm);
      } else {
        actions.player.play();
      }
    } else {
      actions.player.playThisCalm(calm);
    }
  }

  void _pause(AppActions actions) {
    actions.player.pause();
  }

  void _play(AppActions actions, AppState state) {
    if (state.playerState.playingCalm != null) actions.player.play();
  }
}
