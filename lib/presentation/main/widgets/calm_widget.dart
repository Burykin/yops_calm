import 'package:flutter/material.dart';
import 'package:yops_calm/domain/models/models.dart';

class CalmWidget extends StatelessWidget {
  final Calm calm;
  final bool playing;
  final Function(Calm) playThisCalm;
  final Function() pause;

  CalmWidget(
      {this.calm,
      this.playing,
      @required this.playThisCalm,
      @required this.pause});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.3),
        borderRadius: BorderRadius.circular(6),
      ),
      child: Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Icon(Icons.wb_sunny),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(calm.name),
              Text(calm.author),
            ],
          ),
          Expanded(
            child: Container(),
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: playing
                  ? GestureDetector(
                      onTap: () {
                        pause();
                      },
                      child: Icon(Icons.pause))
                  : GestureDetector(
                      onTap: () {
                        playThisCalm(calm);
                      },
                      child: Icon(Icons.play_arrow)))
        ],
      ),
    );
  }
}
