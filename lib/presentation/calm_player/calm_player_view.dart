import 'package:flutter/material.dart';
import 'calm_player_model.dart';

class CalmPlayerView extends StatefulWidget {
  final CalmPlayerModel model;
  final Function() play;
  final Function() pause;
  final Function() open;

  CalmPlayerView(
      {this.model,
      @required this.play,
      @required this.pause,
      @required this.open});

  @override
  CalmPlayerViewState createState() => new CalmPlayerViewState();
}

class CalmPlayerViewState extends State<CalmPlayerView> {
  Duration _duration;
  Duration _position;
  @override
  void initState() {
    _initAudioHandlersPlayer();
    super.initState();
  }

  void _initAudioHandlersPlayer() {
    widget.model.player.audioCache.fixedPlayer.durationHandler =
        (duration) => setState(() {
              _duration = duration;
            });
    widget.model.player.audioCache.fixedPlayer.positionHandler =
        (position) => setState(() {
              _position = position;
            });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          widget.open();
        },
        child: AnimatedContainer(
            duration: Duration(milliseconds: 300),
            height: widget.model.open ? 150 : 50,
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.3),
              borderRadius: BorderRadius.circular(25),
            ),
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Icon(Icons.wb_sunny),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                                "${widget.model.player.playingCalm?.name ?? ""}"),
                            Text(
                                "${widget.model.player.playingCalm?.author ?? ""}"),
                          ],
                        ),
                        Expanded(
                          child: Container(),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Stack(
                                  children: [
                                    Container(
                                        height: 20,
                                        width: 20,
                                        child: CircularProgressIndicator(
                                          value: 1.0,
                                          valueColor: AlwaysStoppedAnimation(
                                              Colors.grey[300]),
                                        )),
                                    Container(
                                        height: 20,
                                        width: 20,
                                        child: CircularProgressIndicator(
                                          value: (_position != null &&
                                                  _duration != null &&
                                                  _position.inMilliseconds >
                                                      0 &&
                                                  _position.inMilliseconds <
                                                      _duration.inMilliseconds)
                                              ? _position.inMilliseconds /
                                                  _duration.inMilliseconds
                                              : 0.0,
                                          valueColor: AlwaysStoppedAnimation(
                                              Colors.cyan),
                                        )),
                                  ],
                                )),
                          ],
                        ),
                        Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: widget.model.player.playing
                                ? GestureDetector(
                                    onTap: () {
                                      widget.pause();
                                    },
                                    child: Icon(Icons.pause))
                                : GestureDetector(
                                    onTap: () {
                                      widget.play();
                                    },
                                    child: Icon(Icons.play_arrow)))
                      ],
                    ),
                    AnimatedOpacity(
                        opacity: widget.model.open ? 1.0 : 0.0,
                        duration: Duration(milliseconds: 500),
                        // The green box needs to be the child of the AnimatedOpacity
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[Icon(Icons.wb_sunny)],
                            ),
                            Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                        "${widget?.model?.playingCalm?.description ?? ""}")
                                  ],
                                )),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: 15, right: 15, top: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(
                                      flex: 3,
                                      child: Container(),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text("Author:"),
                                        Text(
                                            "${widget?.model?.playingCalm?.author ?? ""}"),
                                      ],
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Container(),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text("Name:"),
                                        Text(
                                            "${widget?.model?.playingCalm?.name ?? ""}"),
                                      ],
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Container(),
                                    ),
                                  ],
                                ))
                          ],
                        ))
                  ],
                ))));
  }
}
