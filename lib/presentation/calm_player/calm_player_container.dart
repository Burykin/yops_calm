import 'package:yops_calm/domain/actions/actions.dart';
import 'package:yops_calm/domain/states/states.dart';

import 'package:flutter/material.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';
import 'calm_player_model.dart';
import 'calm_player_view.dart';

class CalmPlayerContainer
    extends StoreConnector<AppState, AppActions, CalmPlayerModel> {
  CalmPlayerContainer({
    Key key,
  }) : super(key: key);

  @override
  CalmPlayerModel connect(AppState state) => CalmPlayerModel(state);

  @override
  Widget build(
      BuildContext context, CalmPlayerModel model, AppActions actions) {
    return CalmPlayerView(
      model: model,
      pause: () {
        _pause(actions);
      },
      play: () {
        _play(actions, model.appState);
      },
      open: () {
        _open(actions);
      },
    );
  }

  void _pause(AppActions actions) {
    actions.player.pause();
  }

  void _play(AppActions actions, AppState state) {
    if (state.playerState.playingCalm != null) actions.player.play();
  }

  void _open(AppActions actions) {
    actions.player.open();
  }
}
