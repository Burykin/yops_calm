import 'package:yops_calm/domain/states/states.dart';
import 'package:yops_calm/domain/models/models.dart';

class CalmPlayerModel {
  final AppState appState;
  CalmPlayerModel(this.appState);
  bool get open => appState.playerState.open;

  Calm get playingCalm => appState.playerState.playingCalm;
  PlayerState get player => appState.playerState;

  bool playing(Calm calm) {
    if (player.playingCalm != null) {
      if (calm.id == player.playingCalm.id && player.playing) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
