// class HomeContainer extends StoreConnector<AppState, AppActions, AppState> {
import 'package:yops_calm/domain/actions/actions.dart';
import 'package:yops_calm/domain/states/states.dart';
import 'package:yops_calm/domain/models/models.dart';
import 'package:yops_calm/app/theme.dart';

import 'package:flutter/material.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';

//   @override
//   connect(AppState state) => state;

//   @override
//   Widget build(BuildContext context, AppState state, AppActions actions) =>
//       HomeView();
// }

typedef OnTabsSelected = void Function(int);

class HomeContainer extends StoreConnector<AppState, AppActions, AppTab> {
  HomeContainer({Key key}) : super(key: key);

  @override
  AppTab connect(AppState state) => state.tabState.activeTab;

  @override
  Widget build(BuildContext context, AppTab activeTab, AppActions action) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: AppTab.toIndex(activeTab),
      onTap: (index) {
        action.tab.updateTabAction(AppTab.fromIndex(index));
      },
      items: AppTab.values.map((tab) {
        return BottomNavigationBarItem(
            icon: Icon(
              AppTab.iconFromName(tab.name),
              color: tabColor(activeTab, tab),
              size: 20,
            ),
            title: Text(
              tabName(tab.name),
              style: TextStyle(color: tabColor(activeTab, tab), fontSize: 10),
            ));
      }).toList(),
    );
  }

  String tabName(String name) {
    switch (name) {
      case 'main':
        return 'Main';
      case 'sleep':
        return 'Sleep';
      case 'Meditate':
        return 'Сообщения';
      case 'music':
        return 'Music';
      default:
        return 'Main';
    }
  }

  Color tabColor(AppTab activeTab, AppTab tab) {
    if (activeTab == tab) {
      return AppTheme.tabBlue;
    } else {
      return AppTheme.tabGrey;
    }
  }
}
