import 'active_tab.dart';
import 'tab_bar_container.dart';
import 'package:yops_calm/domain/models/models.dart';
import 'package:yops_calm/app/theme.dart';
import 'package:yops_calm/presentation/presenters.dart';

import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ActiveTab(
      builder: (BuildContext context, AppTab activeTab) {
        return Scaffold(
            backgroundColor: AppTheme.backgroundDark,
            body: Column(children: <Widget>[
              Expanded(
                  child: Stack(children: <Widget>[
                _body(activeTab),
                _player(),
              ]))
            ]),
            bottomNavigationBar: HomeContainer());
      },
    );
  }

  Widget _body(AppTab activeTab) {
    switch (activeTab.name) {
      case 'main':
        return MainContainer();
      case 'sleep':
        return noPage();
      case 'meditate':
        return noPage();
      case 'music':
        return noPage();
      default:
        return noPage();
    }
  }

  Widget _player() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Padding(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
            child: Row(
              children: <Widget>[Expanded(child: CalmPlayerContainer())],
            ))
      ],
    );
  }

  Widget noPage() {
    return Container(
      color: AppTheme.backgroundDark,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/404.png'),
          ],
        ),
      ),
    );
  }
}
