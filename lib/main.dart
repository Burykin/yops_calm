import 'package:yops_calm/app/app.dart';
import 'package:flutter/material.dart';
import 'package:yops_calm/app/globals.dart' as globals;

void main() async {
  // HttpOverrides.global = new AppHttpOverrides();
  final loggedIn = await globals.initStore();

  runApp(new YopsCalmApp(
    loggedIn: loggedIn,
  ));
}
