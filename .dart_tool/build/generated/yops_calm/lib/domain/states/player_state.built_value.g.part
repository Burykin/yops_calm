// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PlayerState> _$playerStateSerializer = new _$PlayerStateSerializer();

class _$PlayerStateSerializer implements StructuredSerializer<PlayerState> {
  @override
  final Iterable<Type> types = const [PlayerState, _$PlayerState];
  @override
  final String wireName = 'PlayerState';

  @override
  Iterable serialize(Serializers serializers, PlayerState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'audioCache',
      serializers.serialize(object.audioCache,
          specifiedType: const FullType(AudioCache)),
      'playing',
      serializers.serialize(object.playing,
          specifiedType: const FullType(bool)),
      'open',
      serializers.serialize(object.open, specifiedType: const FullType(bool)),
    ];
    if (object.playingCalm != null) {
      result
        ..add('playingCalm')
        ..add(serializers.serialize(object.playingCalm,
            specifiedType: const FullType(Calm)));
    }

    return result;
  }

  @override
  PlayerState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PlayerStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'playingCalm':
          result.playingCalm.replace(serializers.deserialize(value,
              specifiedType: const FullType(Calm)) as Calm);
          break;
        case 'audioCache':
          result.audioCache = serializers.deserialize(value,
              specifiedType: const FullType(AudioCache)) as AudioCache;
          break;
        case 'playing':
          result.playing = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'open':
          result.open = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$PlayerState extends PlayerState {
  @override
  final Calm playingCalm;
  @override
  final AudioCache audioCache;
  @override
  final bool playing;
  @override
  final bool open;

  factory _$PlayerState([void updates(PlayerStateBuilder b)]) =>
      (new PlayerStateBuilder()..update(updates)).build();

  _$PlayerState._({this.playingCalm, this.audioCache, this.playing, this.open})
      : super._() {
    if (audioCache == null) {
      throw new BuiltValueNullFieldError('PlayerState', 'audioCache');
    }
    if (playing == null) {
      throw new BuiltValueNullFieldError('PlayerState', 'playing');
    }
    if (open == null) {
      throw new BuiltValueNullFieldError('PlayerState', 'open');
    }
  }

  @override
  PlayerState rebuild(void updates(PlayerStateBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PlayerStateBuilder toBuilder() => new PlayerStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PlayerState &&
        playingCalm == other.playingCalm &&
        audioCache == other.audioCache &&
        playing == other.playing &&
        open == other.open;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, playingCalm.hashCode), audioCache.hashCode),
            playing.hashCode),
        open.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PlayerState')
          ..add('playingCalm', playingCalm)
          ..add('audioCache', audioCache)
          ..add('playing', playing)
          ..add('open', open))
        .toString();
  }
}

class PlayerStateBuilder implements Builder<PlayerState, PlayerStateBuilder> {
  _$PlayerState _$v;

  CalmBuilder _playingCalm;
  CalmBuilder get playingCalm => _$this._playingCalm ??= new CalmBuilder();
  set playingCalm(CalmBuilder playingCalm) => _$this._playingCalm = playingCalm;

  AudioCache _audioCache;
  AudioCache get audioCache => _$this._audioCache;
  set audioCache(AudioCache audioCache) => _$this._audioCache = audioCache;

  bool _playing;
  bool get playing => _$this._playing;
  set playing(bool playing) => _$this._playing = playing;

  bool _open;
  bool get open => _$this._open;
  set open(bool open) => _$this._open = open;

  PlayerStateBuilder();

  PlayerStateBuilder get _$this {
    if (_$v != null) {
      _playingCalm = _$v.playingCalm?.toBuilder();
      _audioCache = _$v.audioCache;
      _playing = _$v.playing;
      _open = _$v.open;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PlayerState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PlayerState;
  }

  @override
  void update(void updates(PlayerStateBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$PlayerState build() {
    _$PlayerState _$result;
    try {
      _$result = _$v ??
          new _$PlayerState._(
              playingCalm: _playingCalm?.build(),
              audioCache: audioCache,
              playing: playing,
              open: open);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'playingCalm';
        _playingCalm?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PlayerState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
